﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Lab6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        string op;
        int numOne, numTwo;
        int state = 1;


        public MainPage()
        {
            InitializeComponent();
            clearSelected(this, null);
        }

        void clearSelected(object sender, EventArgs e)
        {
            numOne = 0;
            numTwo = 0;
            state = 1;
            this.answerOut.Text = "0";
        }

        void numberSelected(object sender, EventArgs e)
        {
            int number;
            Button button = (Button)sender;
            string selected = button.Text;

            if (this.answerOut.Text == "0" || state < 1)
            {
                this.answerOut.Text = "";
            }

            this.answerOut.Text += selected;

            int.TryParse(this.answerOut.Text, out number);

            if (state == 1)
            {
                numOne = number;
            }
            else
            {
                numTwo = number;
                state = 2;
            }


        }

        void operatorSelected(object sender, EventArgs e)
        {
            state = 0;
            Button button = (Button)sender;
            string selected = button.Text;
            op = selected;
        }

        void equalSelected(object sender, EventArgs e)
        {
            int answer = 0;
            if (state == 2)
            {
                switch (op)
                {
                    case "+":
                        answer = numOne + numTwo;
                        break;

                    case "-":
                        answer = numOne - numTwo;
                        break;

                    case "*":
                        answer = numOne * numTwo;
                        break;

                    case "/":
                        answer = numOne / numTwo;
                        break;
                }

                this.answerOut.Text = answer.ToString();
                numOne = answer;
                state = 0;
            }



        }
    }
}
